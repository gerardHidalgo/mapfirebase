package com.example.gerar.mapapp;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class POIs {

    private int id;
    private String city, name;
    private float latitude, longitude;

    public POIs(){

    }

    public POIs(String city, int id, float latitude, float longitude, String name){
        this.city = city;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public int getId() { return id; }

    public String getCity() { return city; }

    public String getName() { return name; }

    public float getLatitude() { return latitude; }

    public float getLongitude() { return longitude; }

}
